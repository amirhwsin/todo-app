﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Todo_App
{
    public partial class ToDoItem : UserControl
    {
        int p_id;
        public ToDoItem()
        {
            InitializeComponent();
        }

        public ToDoItem(string text, int id = 0)
        {
            InitializeComponent();
            lbl.Text = text;
            p_id = id;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30");
            using (sqlConnection)
            {
                sqlConnection.Open();
                SqlCommand c = new SqlCommand("DELETE FROM dbo.targetTable WHERE id = '"+p_id+"'", sqlConnection);
                c.ExecuteNonQuery();
            }

            this.BackColor = Color.FromArgb(40, 177, 231);
            lbl.Text = "Deleted";
        }
    }
}
