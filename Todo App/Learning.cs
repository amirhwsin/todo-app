﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MouseWheelTest;

namespace Todo_App
{
    public partial class Learning : UserControl
    {
        private ScrollPanelMessageFilter filter;
        public Learning()
        {
            InitializeComponent();
            createTable();
            getTargets();
            ScrollBar vScrollBar1 = new VScrollBar();
            vScrollBar1.Dock = DockStyle.Right;
            vScrollBar1.Scroll += (sender, e) => { panel2.VerticalScroll.Value = vScrollBar1.Value; };
            panel2.Controls.Add(vScrollBar1);
        }

        int poss = 10;

        public void addItem(string text, int id)
        {
            ToDoItem item = new Todo_App.ToDoItem(text, id);
            panel2.Controls.Add(item);
            item.Top = poss;
            poss = (item.Top + item.Height + 10);
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            if (textBox.Text == "")
            {
                MessageBox.Show("عنوان خود را وارد کنید.");
            }
            else
            {
                string tarName = textBox.Text;
                //addItem(tarName);
                addTarget(tarName);
                textBox.Text = "";
            }

        }

        void getTargets()
        {
            SqlConnection sqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;Pooling=False;Connect Timeout=30");
            using (sqlConnection)
            {
                sqlConnection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM dbo.targetTable WHERE tarId = 1", sqlConnection);
                
                using (SqlDataReader reader = command.ExecuteReader()){
                    while (reader.Read()) {
                        addItem("" + reader["tarText"], int.Parse(reader["id"].ToString()));  
                    }

                }
            }
        }

        public void addTarget(string targetTxt)
        {
            SqlConnection sqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30");
            using (sqlConnection)
            {
                sqlConnection.Open();
                string id = getLastRecord();
                SqlCommand c = new SqlCommand("INSERT INTO dbo.targetTable (id, tarText, tarId) VALUES ('"+ id +"', '" + targetTxt + "', '1')", sqlConnection);
                c.ExecuteNonQuery();
                addItem("" + targetTxt, int.Parse(id));
            }
        }
        public void createTable()
        {
            SqlConnection sqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30");
            using (sqlConnection)
            {
                sqlConnection.Open();
                SqlCommand c = new SqlCommand("CREATE TABLE dbo.targetTable (Id INT NOT NULL,tarText VARCHAR(100) NULL,tarId INT NULL)", sqlConnection);
                try
                {
                    c.ExecuteNonQuery();
                }
                catch(Exception e)
                {

                }
            }
        }

        public string getLastRecord()
        {
            SqlConnection sqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;Pooling=False;Connect Timeout=30");
            using (sqlConnection)
            {
                sqlConnection.Open();
                SqlCommand command = new SqlCommand("SELECT TOP 1 id FROM dbo.targetTable ORDER BY Id DESC", sqlConnection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int a = int.Parse(reader["id"].ToString()) + 1;
                        return a.ToString();
                    }

                }
            }
            return "0";
        }
        private void Learning_Load(object sender, EventArgs e)
        {
            
        }
        private void panel2_MouseEnter(object sender, EventArgs e)
        {
            panel2.Focus();
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
